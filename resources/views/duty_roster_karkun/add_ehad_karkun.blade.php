<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title>تمام کیٹیگریز</title>

        @include('inc.head')

        <style>
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
        </style>
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <!--- class="clip-file" -->
                                    <i class=""></i>
                                        تمام عہد  کارکنان
                                </li>
                                <li class="active">
                                </li>
                                <!-- <li class="pull-left" style="margin-left: 30px;">
                                    <a href="admin/attendance">حاضری</a>
                                </li> -->

                            <!--<li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>-->
                            </ol>

                            <div class="page-header">
                                <h1>
                                        تمام عہد  کارکنان
                                    <small style="font-size:25px;margin-right:30px;"></small>
                                </h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->

                        @if(Session::has('msg'))
                            <div class="col-md-12">
                                <div class="alert alert-success">{{Session::get('msg')}}</div>
                            </div>
                        @endif

                        <div class="row">

                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <i class="fa fa-external-link-square"></i>
                                        تمام عہد  کارکنان
                                    </div>
                                     
                                    <div class="panel-body">
                                    
                                    <form method="post" action="{{url('ehadkarkun/add')}}"> 
                                        @csrf 
                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > نام </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="name" id="name" class="form-control" placeholder="کمیٹی کا نام" value="{{old('name')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('name')) {{'نام کا اندراج  ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > ولدیت</span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="fname" id="fname" class="form-control" placeholder="کمیٹی کا نام" value="{{old('fname')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('fname')) {{'والد کا نام ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > رابطہ نمبر </span></label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="phone" id="phone" class="form-control" placeholder="کمیٹی کا نام" value="{{old('phone')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('phone')) {{'فون کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > ایمیل </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="email" id="email" class="form-control" placeholder="کمیٹی کا نام" value="{{old('email')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('email')) {{'ایمیل  کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > شناختی کارڈ نمبر</span></label>
                                                <div>
                                                    <input type="number" autocomplete="off" name="cnic" id="cnic" class="form-control" placeholder="کمیٹی کا نام" value="{{old('cnic')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('cnic')) {{'شناختی  کارڈکا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > مکمل پتہ </span></label>
                                                <div>
                                                    <input type="text" autocomplete="off" name="address" id="address" class="form-control" placeholder="کمیٹی کا نام" value="{{old('address')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('address')) {{'مکمل پتہ کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>



                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" >
                                                
                                                    تاریخ پیدائش

                                                </span></label>
                                                <div>
                                                    <input type="date" autocomplete="off" name="dob" id="dob" class="form-control" placeholder="کمیٹی کا نام" value="{{old('dob')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('dob')) {{' ترایخ پیدائش کا اندراج ضروری ہے۔  '}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" >  تاریخ عہد  </span></label>
                                                <div>
                                                    <input type="date" autocomplete="off" name="doe" id="doe" class="form-control" placeholder="کمیٹی کا نام" value="{{old('doe')}}" >
                                                    @if($errors->any())
                                                        <p>@if($errors->first('doe')) {{'مکمل پتہ کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>
                                        </div>


                                        <div class="row">
                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > شہر</span></label>
                                                <div>
                                                    <select class="select2" name="city">
                                                        <option value="{{old('city')}}">{{old('city')}}</option>
                                                        <option value="Faislabad">Faislabad</option>
                                                        <option value="Lahore">Lahore</option>
                                                    </select>
                                                    @if($errors->any())
                                                        <p>@if($errors->first('city')) {{'شہر کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="symbol required" > ملک</span></label>
                                                <div>
                                                    <select class="select2" name="country">
                                                        <option value="{{old('country')}}">{{old('country')}}</option>
                                                        <option value="Pak">Pak</option>
                                                        <option value="Turkey">Turkey</option>
                                                    </select>
                                                    @if($errors->any())
                                                        <p>@if($errors->first('country')) {{'ملک کا اندراج ضروری ہے۔'}} @endif </p>
                                                    @endif
                                                </div>
                                            </div>
                                          </div>

                                          <div class="col-sm-4">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12 @if($errors->any()) has-error @endif">
                                                <label class="control-label"><span class="" > معرفت </span></label>

                                                <div>
                                                    <input type="text" autocomplete="off" name="marfat" id="marfat" class="form-control" value="{{old('marfat')}}" >
                                                </div>

                                            </div>
                                          </div>
                                        </div>


                                        <div class="row">
                                          <div class="col-sm-6">
                                            <!-- has-error -->
                                            <div class="form-group col-sm-12">
                                                <input type="submit" class="btn btn-sm btn-primary" name="submit" value="اندراج کریں">
                                            </div>

                                          </div>
                                        </div>

                                    </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

<!-- end: PAGE CONTENT-->
</div>
</div>
            <!-- end: PAGE -->
</div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
            });
            $(document).ready(function(){
                $(".btn_submit_P").click(function(){
                    $(".HazriKarkoonatten").val("P");
                });

                $(".btn_submit_L").click(function(){
                    $(".HazriKarkoonatten").val("L");
                });             

                $(".btn_submit_M").click(function(){
                    $(".HazriKarkoonatten").val("M");
                });
        });
        </script>
    </body>
    <!-- end: BODY -->
<script>
    $(document).ready(function() {
        $(document).find("#name").focus();
    });
</script>

</html>