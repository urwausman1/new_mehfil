        <div class="footer clearfix">
            <!-- <div class="footer-inner">Prowered by idreesia ahad Kameti.</div> -->
            <div class="footer-items">
                <span class="go-top"><i class="clip-chevron-up"></i></span>
            </div>
        </div>

        <!-- end: FOOTER -->

        <!-- start: MAIN JAVASCRIPTS -->
        <script src="{{asset('assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js')}}"></script>
        
        <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        
        <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/blockUI/jquery.blockUI.js')}}"></script>
        
        <script src="{{asset('assets/plugins/iCheck/jquery.icheck.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js')}}"></script>
        
        <script src="{{asset('assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.js')}}"></script>
        
        <script src="{{asset('assets/plugins/less/less-1.5.0.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js')}}"></script>
        
        <script src="{{asset('assets/js/main.js')}}"></script>
        
        <!-- end: MAIN JAVASCRIPTS -->
        
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        
        <script src="{{asset('assets/plugins/jquery-inputlimiter/jquery.inputlimiter.1.3.1.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/autosize/jquery.autosize.min.js')}}"></script>        
        
        <script src="{{asset('assets/plugins/select2/select2.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/jquery.maskedinput/src/jquery.maskedinput.js')}}"></script>
        
        <script src="{{asset('assets/plugins/jquery-maskmoney/jquery.maskMoney.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-daterangepicker/moment.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-colorpicker/js/commits.js')}}"></script>
        
        <script src="{{asset('assets/plugins/jQuery-Tags-Input/jquery.tagsinput.js')}}"></script>
        
        <script src="{{asset('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/summernote/build/summernote.min.js')}}"></script>
        
        <script src="{{asset('assets/plugins/ckeditor/ckeditor.js')}}"></script>
        
        <script src="{{asset('assets/plugins/ckeditor/adapters/jquery.js')}}"></script>
        
        <script src="{{asset('assets/js/form-elements.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('assets/plugins/DataTables/media/js/jquery.dataTables.min.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('assets/plugins/DataTables/media/js/DT_bootstrap.js')}}"></script>
        
        <script src="{{asset('assets/js/table-data.js')}}"></script>
        <!-- start: JAVASCRIPTS Form validation -->
        
        <script src="{{asset('assets/plugins/jquery-validation/dist/jquery.validate.min.js')}}"></script>
        
        <script src="{{asset('assets/js/form-validation.js')}}"></script>
        
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        
        <script src="{{asset('assets/js/bootstrap-multiselect.js')}}"></script>

        <script>

        CKEDITOR.env.isCompatible = true;

        jQuery(document).ready(function() {
            Main.init();
            FormElements.init();
            FormValidator.init();
            TableData.init();
        });
        </script>
         
         <script src="{{asset('assets/js/custom.js')}}"></script>