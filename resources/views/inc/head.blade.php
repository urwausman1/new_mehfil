<!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/fonts/style.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/css/main-responsive.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/iCheck/skins/all.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/perfect-scrollbar/src/perfect-scrollbar-rtl.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/css/theme_light.css')}}" type="text/css" id="skin_color">
        
        <link rel="stylesheet" href="{{asset('assets/css/print.css')}}" type="text/css" media="print"/>
        
        <link rel="stylesheet" href="{{asset('assets/css/rtl-version.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-multiselect.css')}}">
        <!--[if IE 7]>
        <link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
        <![endif]-->
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        
        <link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/datepicker/css/datepicker.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/jQuery-Tags-Input/jquery.tagsinput.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/summernote/build/summernote.css')}}">
        
        <link rel="stylesheet" href="{{asset('assets/plugins/DataTables/media/css/DT_bootstrap.css')}}" />
        
        <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}" />
        
        <script src="{{asset('assets/js/jquery.js')}}"></script>

        <style>
        .badge {
            background-color:#007aff;
        }
        </style>