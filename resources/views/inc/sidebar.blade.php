                <div class="main-navigation navbar-collapse collapse">
                    <!-- start: MAIN MENU TOGGLER BUTTON -->
                    <div class="navigation-toggler">
                        <i class="clip-chevron-left"></i>
                        <i class="clip-chevron-right"></i>
                    </div>
                    <!-- end: MAIN MENU TOGGLER BUTTON -->

                    <!-- start: MAIN NAVIGATION MENU -->
                    <ul class="main-navigation-menu">
                        

                        <li class="active">
                            <a href="{{url('dashboard')}}"><i class="clip-home-3"></i>
                                <span class="title"> خو ش  آمد ید </span><span class="selected"></span>
                            </a>
                        </li>
                        
                        <li class="">
                            <a href="#"><i class="clip-home-3"></i>
                                <span class="title"> نو ٹس بورڈ </span><span class=""></span>
                            </a>
                        </li>

                        @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser' || session('mehfil') == 'mehfil')

                        <li class="">
                            <a href="{{url('karkun')}}"><i class="clip-home-3"></i>
                                <span class="title"> کارکنان </span><span class=""></span>
                            </a>
                        </li>

                        @endif

                        @if(session('user_role') == 'ehdkarkun' || session('user_role') == 'superuser')

                        <li class="">
                            <a href="{{url('mahafil')}}"><i class="clip-home-3"></i>
                                <span class="title"> محافل</span><span class=""></span>
                            </a>
                        </li>
                        @endif

                        @if(session('user_role') == 'superuser')
                        <li class="">
                            <a href="{{url('ehadkarkun')}}"><i class="clip-home-3"></i>
                                <span class="title"> عہد کارکنان </span><span class=""></span>
                            </a>
                        </li>
                        @endif
                        
                        <li class="">
                            <a href="{{url('dutyrosterkarkun')}}"><i class="clip-home-3"></i>
                                <span class="title"> ڈیوٹی روسٹر </span><span class=""></span>
                            </a>
                        </li>
                        
                        <li class="">
                             <a href="#"><i class="clip-home-3"></i>
                                <span class="title">   کارکنان حاضری </span><span class="" ></span>
                            </a>
                        </li>
                       
                        <li class="">
                             <a href="#"><i class="clip-home-3"></i>
                                <span class="title">کارکنان حاضری اسٹیٹس  </span><span class="" ></span>
                            </a>

                        </li>

                        <li class="">
                            <a href="#"><i class="clip-home-3"></i>
                            <span class="title">عہد کمیٹی </span><span class="" ></span>
                            </a>
                        </li>
                        
                        <!--<li>
                            <a href="index.html"><i class="clip-home-3"></i>
                                <span class="title"> اخراجات </span><span ></span>
                            </a>
                        </li>-->
                        
                        <li class="">
                            <a href="#"><i class="clip-home-3"></i>
                                <span class="title">   عہد رجسٹر  </span><span class=""  ></span>
                            </a>
                        </li>   
                        
                        <li class="">
                            <a href="{{url('help')}}"><i class="clip-home-3"></i>
                                <span class="title">     اردو کی بورڈ انسٹا لیشن کا طریقہ  </span><span class="">
                                </span>
                            </a>
                        </li>


                        <li class="">
                            <a href="{{url('committees')}}"><i class="clip-home-3"></i>
                                <span class="title">کمیٹیاں</span><span class="">
                                </span>
                            </a>
                        </li>

                        <li class="">
                            <a href="{{url('finance')}}"><i class="clip-home-3"></i>
                                <span class="title">فنانس</span><span class="">
                                </span>
                            </a>
                        </li>

                         <li>
                             <a href="{{url('logout')}}"> <i class="clip-home-3"></i>
                                <span class="title"> لا گ آو ٹ </span><span ></span>
                            </a>
                        </li>

                    </ul>
                    <!-- end: MAIN NAVIGATION MENU -->
                </div>
