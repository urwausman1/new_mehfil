<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title>تمام کیٹیگریز</title>

        @include('inc.head')

        <style>
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
        </style>
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <!--- class="clip-file" -->
                                    <i class=""></i>
                                        مدد
                                </li>
                                <li class="active">
                                </li>
                                <!-- <li class="pull-left" style="margin-left: 30px;">
                                    <a href="admin/attendance">حاضری</a>
                                </li> -->

                            <!--<li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>-->
                            </ol>

                            <div class="page-header">
                                <h1>
                                    مدد
                                    <small style="font-size:25px;margin-right:30px;"></small>
                                </h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
             
        <div class="panel-body">
         
<div class="row">
    <div class="col-md-12">
            ِ<h4 style="font-weight:bold;">Windows 8, 7 &amp; Vista میں اردو میں ٹائپ اور تلاش کریں</h4>
      <p style="font-size:16px;">

           ۱۔ اردو کی بورڈ   <a target="_blank" href="https://urdu.ca/2">یہاں سے</a> ڈائون لوڈ کریں۔<br>

            ۲۔ ڈائون لوڈ کی ہوئی فائل کو انسٹال کریں۔<br>

               فائل انسٹال ہونے کے بعد نیچے دائیں جانب زبان نظر آنی شروع ہو جائے گی۔<br>

               <img src="http://localhost/mehfil/assets/images/language-bar.jpg"><br>

               ۳۔ انگلش سے اردو کی بورڈ سیلکٹ کرنے کے لیے  LEFT SHIFT + ALT دبائیں۔<br>

               ۴۔ آپ اردو کی بورڈ کی تصویر یہاں سے ڈاون لوڈ کر سکتے ۔ جس سے آپ کو ٹائپ کرنے میں آسانی ہو گی۔<br>

               آپ کی آسانی کے لیے اردو کی بورڈ کی تصویر نیچے بھی منسلک ہے۔<br>

               <img src="http://localhost/mehfil/assets/images/keyboard.gif"><br>

               ۵۔ دوبارو انگلش سیلکٹ کرنے کے لیے LEFT SHIFT + ALT دبائیں۔<br>
      </p>
    </div>
</div>

        </div>
</div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>

<!-- end: PAGE CONTENT-->
</div>
</div>
            <!-- end: PAGE -->
</div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
            });
            $(document).ready(function(){
                $(".btn_submit_P").click(function(){
                    $(".HazriKarkoonatten").val("P");
                });

                $(".btn_submit_L").click(function(){
                    $(".HazriKarkoonatten").val("L");
                });             

                $(".btn_submit_M").click(function(){
                    $(".HazriKarkoonatten").val("M");
                });
        });
        </script>
    </body>
    <!-- end: BODY -->
</html>