<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title>KARKUN Portal</title>
        @include('inc.head')
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                	<!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>


            <!-- start: PAGE -->
            <div class="main-content">
                <!-- start: PANEL CONFIGURATION MODAL FORM -->
                <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    &times;
                                </button>
                                <h4 class="modal-title">Panel Configuration</h4>
                            </div>

                            <div class="modal-body">
                                Here will be a configuration form
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                    Close
                                </button>

                                <button type="button" class="btn btn-primary">
                                    Save changes
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
                <!-- end: SPANEL CONFIGURATION MODAL FORM -->

                <div class="container">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <!--- class="clip-file" -->
                                    <i class=""></i>
                                        خو ش  آمد ید  
                                </li>
                                <li class="active"></li>                                
                                <li class="pull-left" style="margin-left: 30px;">
                                    <a href="#">حاضری</a>
                                </li>
                            </ol>

                            <div class="page-header">
                                <h1>  خو ش  آمد ید <small style='font-size:25px;margin-right:30px;'></small></h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->

                    <div class="row">
                    	<div class="col-md-12">
                        	<!-- <div class="col-md-6">
                        		<div class="jumbotron" style="font-size: 26px;">
                                	نئے اور پرا نے ٰعہد <span class="badge" style="float:left; margin-top:5px; font-size:18px;">0</span>
                                </div>
                        	</div> -->
                            <div class="col-md-6">
                                <div class="jumbotron" style="font-size: 26px;">
                                    کا ر کنا ن حا ضری اسٹیٹس <span class="badge" style="float:left; margin-top:5px; font-size:18px;">55%</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                      			<div class="jumbotron" style="font-size: 26px;">
                                	ٹو ٹل کا ر کنا ن<span class="badge" style="float:left; margin-top:5px; font-size:18px;">0</span>
                                </div>
                        	</div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                      			<div class="jumbotron" style="font-size: 26px;">
                                	عہد کمیٹی کی تعداد<span class="badge" style="float:left; margin-top:5px; font-size:18px;">0</span>
                                </div>
                        	</div>
                        </div>
                    </div>
                    <!-- end: PAGE CONTENT-->
                </div>
            </div>
            <!-- end: PAGE -->
        </div>
        <!-- end: MAIN CONTAINER -->

        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
			$(document).ready(function() {
				$('#example-getting-started').multiselect();
			});
    		$(document).ready(function(){
    			$(".btn_submit_P").click(function(){
    				$(".HazriKarkoonatten").val("P");
    			});

				$(".btn_submit_L").click(function(){
    				$(".HazriKarkoonatten").val("L");
				});				

				$(".btn_submit_M").click(function(){
    				$(".HazriKarkoonatten").val("M");
				});
		});
		</script>
    </body>
    <!-- end: BODY -->
</html>