<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>

		<title>KARKUN Portal</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<!-- start: MAIN CSS -->
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet"> 
        <link rel="stylesheet" href="assets/css/bodylogin.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/fonts/style.css">
		<link rel="stylesheet" href="assets/css/main.css">
		<link rel="stylesheet" href="assets/css/main-responsive.css">
		<link rel="stylesheet" href="assets/plugins/iCheck/skins/all.css">
		<link rel="stylesheet" href="assets/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
		<link rel="stylesheet" href="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
		<link rel="stylesheet" href="assets/css/theme_light.css" type="text/css" id="skin_color">
		<link rel="stylesheet" href="assets/css/print.css" type="text/css" media="print"/>

		<style>
		.help-block{
			font-size: 13px !important;
		    font-family: 'Josefin Sans', sans-serif !important;
			font-style:italic;
			font-weight: bold;
		}
		.errorHandler{
			 font-family: 'Josefin Sans', sans-serif !important;
			 font-size: 17px;
		}
		.alert-danger {
		    background-color: #f2dede;
		    border-color: #ebccd1;
		    color: #a94442;
		    font-style: italic !important;
			text-align:center !important;
		}
		</style>
	</head>

	<!-- end: HEAD -->

	<!-- start: BODY -->

	<body class="login example1" >

		<div class="main-login col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
			<div class="logo" style="color:#f5f5f5; font-family: 'Josefin Sans', sans-serif !important; font-size:30px; color:black;">KARKUN Portal
			</div>
			@if(Session::has('msg'))
				<div class="col-md-12">
					<div class="alert alert-success">{{Session::get('msg')}}</div>
				</div>
			@endif
            <!-- start: LOGIN BOX -->
			<div class="box-login">
				<h3 style="font-family: 'Josefin Sans', sans-serif !important; text-align: center;">
					Sign in to your account
				</h3>
				
				<p style="font-family: 'Josefin Sans', sans-serif;font-size: 10px;"> 
					Please enter your email and password to log in. 
				</p>

				<form class="form-login" action="{{url('login')}}" method="post">
				    @csrf
				    <div class="errorHandler alert alert-danger no-display"> 
				    	<i class="fa fa-remove-sign"></i> 
				    	You have some form errors. Please check below. 
					</div>    
			    	
			    	<fieldset>
						<div class="form-group"> <span class="input-icon">

					        <input type="text" id="username" class="form-control" name="username" value="{{old('username')}}" placeholder="User Name..." required="required" style="font-family: 'Josefin Sans', sans-serif;">
							
							<i class="fa fa-envelope"></i> </span> 
							<!-- To mark the incorrectly filled input, you must add the class "error" to the input --> 
							<!-- example: <input type="text" class="login error" name="login" value="Username" /> --> 
			      		</div>

					<div class="form-group form-actions"> 

						<span class="input-icon">
			        	
			        		<input type="password" class="form-control password" value="{{old('pwd')}}" name="pwd" placeholder="Password" required="required" style="font-family: 'Josefin Sans', sans-serif;">

				        	<i class="fa fa-lock"></i><!--<a class="forgot" href="auth/forgot_password"> I forgot my password </a>--> 
				     	</span> 
					</div>
			        
			        <div class="form-group form-actions">
				        <button type="submit" class="btn btn-success col-md-12 form-control" style="font-family: 'Josefin Sans', sans-serif;">
				        	Login <i class="fa fa-arrow-circle-right"></i> 
				        </button>
			      	</div>

					<div class="new-account">
			   		<!--<h4 style="text-align:center; font-family: 'Josefin Sans', sans-serif; font-size: 25px;"> <b>Feed Back Contact 0312 - 381 66 66 </b></h4>-->
			      	</div>
			    </fieldset>
			  </form>

			</div>
			<!-- end: LOGIN BOX --> 

            <div style="margin-top: 50px;text-align: center;">
            	<a href="https://urdu.ca/Phonetic-Keyboard.exe">Download Urdu Keyboard</a>
            </div>
			<!-- start: COPYRIGHT -->
			<div class="copyright"></div>
			<!-- end: COPYRIGHT -->
		</div>


		<script src="assets/js/jquery.js"></script>
		<!--<![endif]-->
		<script src="assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
		<script src="assets/plugins/blockUI/jquery.blockUI.js"></script>
		<script src="assets/plugins/iCheck/jquery.icheck.min.js"></script>
		<script src="assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
		<script src="assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
		<script src="assets/plugins/less/less-1.5.0.min.js"></script>
		<script src="assets/plugins/jquery-cookie/jquery.cookie.js"></script>
		<script src="assets/plugins/bootstrap-colorpalette/js/bootstrap-colorpalette.js"></script>
		<script src="assets/js/main.js"></script>
		<!-- end: MAIN JAVASCRIPTS -->
		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
		<script src="assets/js/login.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
			jQuery(document).ready(function() {
				Main.init();
				Login.init();
			});
		</script>
	</body>
	<!-- end: BODY -->
</html>

