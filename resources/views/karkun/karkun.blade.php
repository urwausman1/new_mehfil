<!DOCTYPE html>
<html lang="en" class="no-js">
    <!-- start: HEAD -->
    <head>
        <title>تمام کیٹیگریز</title>

        @include('inc.head')

        <style>
        #contacts_table_wrapper .select2-container{
            width: 100px;
        }
        #contacts_table_wrapper .select2-container span.select2-chosen{
            text-align: right;
        }
        table.table thead .sorting{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_both.png') no-repeat center left;
        }
        table.table thead .sorting_asc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_asc.png') no-repeat center left;
        }
        table.table thead .sorting_desc{
            text-align: right;
            background: url('assets/plugins/DataTables/media/css/images/sort_desc.png') no-repeat center left;
        }
        </style>
    </head>

    <!-- end: HEAD -->

    <!-- start: BODY -->
    <body class="rtl">    
        <!-- start: HEADER -->
        <div class="navbar navbar-inverse navbar-fixed-top">
            <!-- start: TOP NAVIGATION CONTAINER -->
            <div class="container">
                <div class="navbar-header">
                    <!-- start: RESPONSIVE MENU TOGGLER -->
                    <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                        <span class="clip-list-2"></span>
                    </button>
                    <!-- end: RESPONSIVE MENU TOGGLER -->
                    <!-- end: LOGO -->
                </div>
            </div>
            <!-- end: TOP NAVIGATION CONTAINER -->
        </div>
        <!-- end: HEADER -->

        <!-- start: MAIN CONTAINER -->
        <div class="main-container">
            <div class="navbar-content">
                <!-- start: SIDEBAR -->
                @include('inc.sidebar')
                <!-- end: SIDEBAR -->
            </div>
            <!-- start: PAGE -->
            <div class="main-content">

                <div class="container" style="min-height: 760px;">
                    <!-- start: PAGE HEADER -->
                    <div class="row">
                        <div class="col-sm-12">
                            <!-- start: PAGE TITLE & BREADCRUMB -->
                            <ol class="breadcrumb">
                                <li>
                                    <!--- class="clip-file" -->
                                    <i class=""></i>
عہد  کارکنان
                                </li>
                                <li class="active">
                                </li>
                                <!-- <li class="pull-left" style="margin-left: 30px;">
                                    <a href="admin/attendance">حاضری</a>
                                </li> -->

                            <!--<li class="search-box">
                                    <form class="sidebar-search">
                                        <div class="form-group">
                                            <input type="text" placeholder="Start Searching...">
                                            <button class="submit">
                                                <i class="clip-search-3"></i>
                                            </button>
                                        </div>
                                    </form>
                                </li>-->
                            </ol>

                            <div class="page-header">
                                <h1>
عہد  کارکنان
                                    <small style="font-size:25px;margin-right:30px;"></small>
                                </h1>
                            </div>
                            <!-- end: PAGE TITLE & BREADCRUMB -->
                        </div>
                    </div>
                    <!-- end: PAGE HEADER -->
                    <!-- start: PAGE CONTENT -->
                    
<div class="row">
<div class="col-md-12">
    <!-- start: DYNAMIC TABLE PANEL -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-external-link-square"></i>
عہد  کارکنان
        </div>

         
        <div class="panel-body">
         
                        @if(Session::has('msg'))
                            <div class="col-md-12">
                                <div class="alert alert-success">{{Session::get('msg')}}</div>
                            </div>
                        @endif
                        
            <a class="btn btn-primary" role="button" href="{{url('karkun/add')}}" style="position: relative;float: left;">
                نیا اضافہ&lrm; 
                <div style="background-color: #428bca;padding-left: 5px;padding-right: 5px;margin-top: 0px;float: left;margin-left: -5px;margin-right: 5px;">+
                </div>
            </a>
                
            <!-- <div class="col-md-12">
              <div class="errorHandler alert alert-danger no-display">
                <button data-dismiss="alert" class="close"> × </button>
                <i class="fa fa-times-sign"></i> معلومات درست نہیں </div>

              <div class="successHandler alert alert-success no-display">
                <button data-dismiss="alert" class="close"> × </button>
                <i class="fa fa-ok"></i> معلومات کا اندراج کامیاب رہا&#8236;&lrm; </div>
            </div> -->

       

        <div class="table-responsive">
         <table class="table-bordered" width="100%">
             <thead></thead>
         </table>
         <br><br>

        <div id="contacts_table_wrapper" class="dataTables_wrapper form-inline" role="grid">

            <table class="table table-striped table-bordered table-hover table-full-width" id="contacts_table">
                
                <thead style="font-size: 19px !important;">
                    <tr>
                        <th>نام</th>
                        <th>ولدیت</th>
                        <th>معرفت</th>
                        <th>شناختی کارڈ نمبر</th>
                        <th>عمر</th>
                        <th> عرصہ عہد</th>
                        <th>رابطہ نمبر</th>
                        <th>پتہ</th>
                        <th>شہر</th>
                        <th> ملک</th>
                        <th width="10%">  ایکشن</th>
                    </tr>
                </thead>

                <tbody>
                    <?php 

function getAge($dob,$condate){ 
    $birthdate = new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $dob))))));
    $today= new DateTime(date("Y-m-d",  strtotime(implode('-', array_reverse(explode('/', $condate))))));           
    $age = $birthdate->diff($today)->y;

    return $age;
}

                    ?>


                        @foreach($all_karkun as $single_karkun)
                        <tr>

                            <td>{{$single_karkun->kp_name }}</td>
                            <td>{{$single_karkun->kp_fname }}</td>
                            <td>{{$single_karkun->kp_marfat }}</td>
                            <td>{{$single_karkun->kp_cnic }}</td>
                            <td>
                                <?php 
                                    $dob= $single_karkun->kp_dob; //date of Birth
                                    $condate= date('Y-m-d'); //Certain fix Date of Age 
                                    echo getAge($dob,$condate) . ' سال';
                                ?>
                            </td>
                            <td>
                                <?php 
                                    $dob= $single_karkun->kp_doe; //date of Birth
                                    $condate= date('Y-m-d'); //Certain fix Date of Age 
                                    echo getAge($dob,$condate) . ' سال';
                                ?>
                            </td>
                            <td>{{$single_karkun->kp_phone }}</td>
                            <td>{{$single_karkun->kp_address }}</td>
                            <td>{{$single_karkun->kp_city }}</td>
                            <td>{{$single_karkun->kp_country }}</td>

                            <td width="15%">
                                <a href="{{url('karkun/edit/'.$single_karkun->id)}}">تبدیلی</a>
                                <br>

                                <a href="{{url('karkun/delete/'.$single_karkun->id)}}">حذف</a>
                            </td>                    
                        </tr>
                        @endforeach
                </tbody>
        </table>

    </div>
</div>
</div>
</div>
    <!-- end: DYNAMIC TABLE PANEL -->
</div>
</div>

<!-- end: PAGE CONTENT-->
</div>
</div>
            <!-- end: PAGE -->
</div>
        <!-- end: MAIN CONTAINER -->



        <!-- start: FOOTER & scripts -->
            @include('inc.footer')
        
         <script type="text/javascript">
            $(document).ready(function() {
                $('#example-getting-started').multiselect();
            });
            $(document).ready(function(){
                $(".btn_submit_P").click(function(){
                    $(".HazriKarkoonatten").val("P");
                });

                $(".btn_submit_L").click(function(){
                    $(".HazriKarkoonatten").val("L");
                });             

                $(".btn_submit_M").click(function(){
                    $(".HazriKarkoonatten").val("M");
                });
        });
        </script>
    </body>
    <!-- end: BODY -->
</html>