<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\CommitteesController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\EhadKarkunController;
use App\Http\Controllers\MehfilController;
use App\Http\Controllers\KarkunController;
// use App\Http\Controllers\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// route to show the login form
Route::get('login', [UserController::class, 'showLogin']);
// route to process the form
Route::post('login', [UserController::class, 'doLogin']);


Route::get('dashboard', [DashboardController::class, 'showDashboard']);
Route::get('logout', [UserController::class, 'doLogout']);


// to perform complete crud function in committee table
Route::get('committees', [CommitteesController::class, 'showData']);
Route::get('committees/add', [CommitteesController::class, 'addCommitteesPage']);
Route::post('committees/add', [CommitteesController::class, 'insertCommittee']);
Route::get('committees/edit/{id}', [CommitteesController::class, 'editCommittee']);
Route::post('committees/edit/{id}', [CommitteesController::class, 'updateCommittee']);
Route::get('committees/delete/{id}', [CommitteesController::class, 'delCommittee']);

// to perform complete crud function in finance table
Route::get('finance', [FinanceController::class, 'showData']);
Route::get('finance/add', [FinanceController::class, 'addFinancePage']);
Route::post('finance/add', [FinanceController::class, 'insertFinance']);
Route::get('finance/edit/{id}', [FinanceController::class, 'editFinance']);
Route::post('finance/edit/{id}', [FinanceController::class, 'updateFinance']);
Route::get('finance/delete/{id}', [FinanceController::class, 'delFinance']);


// to perform complete crud function in ehad_karkun table
Route::get('ehadkarkun', [EhadKarkunController::class, 'showData']);
Route::get('ehadkarkun/add', [EhadKarkunController::class, 'addEhadKarkunPage']);
Route::post('ehadkarkun/add', [EhadKarkunController::class, 'insertEhadKarkun']);
Route::get('ehadkarkun/edit/{id}', [EhadKarkunController::class, 'editEhadKarkun']);
Route::post('ehadkarkun/edit/{id}', [EhadKarkunController::class, 'updateEhadKarkun']);
Route::get('ehadkarkun/delete/{id}', [EhadKarkunController::class, 'delEhadKarkun']);


// to perform complete crud function in mehfils table
Route::get('mahafil', [MehfilController::class, 'showData']);
Route::get('mahafil/add', [MehfilController::class, 'addMehfilPage']);
Route::post('mahafil/add', [MehfilController::class, 'insertMehfil']);
Route::get('mahafil/edit/{id}', [MehfilController::class, 'editMehfil']);
Route::post('mahafil/edit/{id}', [MehfilController::class, 'updateMehfil']);
Route::get('mahafil/delete/{id}', [MehfilController::class, 'delMehfil']);


// to perform complete crud function in karkun table
Route::get('karkun', [KarkunController::class, 'showData']);
Route::get('karkun/add', [KarkunController::class, 'addKarkunPage']);
Route::post('karkun/add', [KarkunController::class, 'insertKarkun']);
Route::get('karkun/edit/{id}', [KarkunController::class, 'editKarkun']);
Route::post('karkun/edit/{id}', [KarkunController::class, 'updateKarkun']);
Route::get('karkun/delete/{id}', [KarkunController::class, 'delKarkun']);


Route::get('dutyrosterkarkun', function(){
	return view('duty_roster_karkun.dutyroster');
});









Route::get('help', function () {
    return view('help');
});
