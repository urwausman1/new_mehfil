<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Karkun extends Model
{
    use HasFactory;

    protected $fillable = ['kp_name' , 'kp_fname' , 'kp_address' , 'kp_city' , 'kp_country' , 'kp_cnic' , 'kp_phone' , 'kp_email' , 'kp_dob' , 'kp_doe' , 'kp_notes' , 'kp_marfat' , 'committee_id' , 'mehfil_id' ];

}
