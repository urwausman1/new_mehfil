<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EhadKarkun extends Model
{
    use HasFactory;

    protected $fillable = ['ekp_name' , 
    						'ekp_fname' , 
    						'ekp_phone' , 
    						'ekp_email' , 
    						'ekp_address' , 
    						'ekp_city' , 
    						'ekp_country' , 
    						'ekp_cnic'
                            'ekp_dob'
                            'ekp_doe'
                            'ekp_marfat'
    					];
}
