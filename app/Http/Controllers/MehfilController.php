<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Mehfil;

class MehfilController extends Controller{

    public function showData(){

        $all_mehfil = DB::table('mehfils')->get();
        return view('mahafil.mehfil')->with(compact('all_mehfil'));
    }
    
    public function addMehfilPage(){
        
        return view('mahafil.add_mehfil');
    }

    public function insertMehfil(Request $request){
        
        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'number' => 'required',
            'phone' => 'required',
            'mediaphone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data_admin = array(
                        'admin_username' => $request->number , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'mehfil' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            $mehfil_code = $request->number."_".$request->city."_".$request->country;
            $data = array(
                        'mehfil_code' => $mehfil_code, 
                        'mehfil_name' => $request->name, 
                        'mehfil_number' => $request->number, 
                        'mehfil_mobile' => $request->phone, 
                        'mehfil_media_cell_phone' => $request->mediaphone, 
                        'mehfil_email' => $request->email, 
                        'mehfil_address' => $request->address, 
                        'mehfil_city' => $request->city, 
                        'mehfil_country' => $request->country, 
                        'mehfil_coordinates' => $request->coordinates, 
                        'admin_id' => $last->id, 
                    );
    
            DB::table('mehfils')->insert($data);

            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('mahafil/add');

        }else{
            return redirect('mahafil/add')->withErrors($validator)->withInput();
        }
    }


    function editMehfil($id, Request $request){

        $single_mehfil = DB::table('mehfils')->where('id', $id)->first();

        if (!$single_mehfil) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('mahafil');
        }else{
            return view('mahafil.edit_mehfil')->with(compact('single_mehfil'));
        }
    
    }

    function updateMehfil($id , Request $request){



        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'number' => 'required',
            'phone' => 'required',
            'mediaphone' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
        ]);
        
        if ($validator->passes()) {


            $mehfil_code = $request->number."_".$request->city."_".$request->country;
            $data = array(
                        'mehfil_code' => $mehfil_code, 
                        'mehfil_name' => $request->name, 
                        'mehfil_number' => $request->number, 
                        'mehfil_mobile' => $request->phone, 
                        'mehfil_media_cell_phone' => $request->mediaphone, 
                        'mehfil_email' => $request->email, 
                        'mehfil_address' => $request->address, 
                        'mehfil_city' => $request->city, 
                        'mehfil_country' => $request->country, 
                        'mehfil_coordinates' => $request->coordinates, 
                    );
    
            DB::table('mehfils')->where('id' , $id)->update($data);

            // get the last id to update the data
            $getIdForUpdate = DB::table('mehfils')->where('id' , $id)->first();


            $data_admin = array(
                        'admin_username' => $request->number , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'mehfil' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->where('id' , $getIdForUpdate->admin_id)->update($data_admin);

            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');    
            return redirect('mahafil');

        }else{
            return redirect('mahafil/edit/'.$id)->withErrors($validator)->withInput();
        }
    }
    

}
