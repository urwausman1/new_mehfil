<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function showLogin(){
        return view('login');
    }

    public function doLogin(Request $request){

        $validator = Validator::make($request->all() , [
            'username' => 'required',
            'pwd' => 'required'
        ]);


        if ($validator->passes()) {

            $user = DB::table('users')
                        ->where('admin_username', $request->username)
                        ->where('admin_password', $request->pwd)
                        ->first();
            if ($user) {

                $request->session()->put('userid' , $user->id);
                $request->session()->put('user_role' , $user->admin_access_level);
                return redirect('dashboard');

            }else{
                $request->session()->flash('msg' , 'username or password incorrect');
                return redirect('login')->withErrors($validator)->withInput();
            }
            // if (Auth::attempt($userdata)) {

                // validation successful!
                // redirect them to the secure section or whatever
                // return Redirect::to('secure');
                // for now we'll just echo success (even though echoing in a controller is bad)
                // echo 'SUCCESS!';

            // }else{
            //     return redirect('login')->withErrors($validator)->withInput();
            // }
        }else{
            return redirect('login')->withErrors($validator)->withInput();
        }
    }

    public function doLogout(){
        // Auth::logout(); // log the user out of our application
        
        if (session()->has('user_role')) {
            session()->pull('user_role');
            return redirect('login'); // redirect the user to the login screen
        }

        return redirect('login'); // redirect the user to the login screen
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
