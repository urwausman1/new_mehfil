<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Finance;

class FinanceController extends Controller{

    public function showData(){

        $all_finance = DB::table('finances')->get();
        return view('finance.finance')->with(compact('all_finance'));
    }

    
    public function addFinancePage(){
        return view('finance.add_finance');
    }


    public function insertFinance(Request $request){

        $validator = Validator::make($request->all() , [
            'name' => 'required',
        ]);
        
        if ($validator->passes()) {
            $data = array(
                        'finance_name' => $request->name, 
                    );
    
            DB::table('finances')->insert($data);

            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('finance/add');

        }else{
            return redirect('finance/add')->withErrors($validator)->withInput();
        }
    }


    function editFinance($id, Request $request){

        $finance = DB::table('finances')->where('id', $id)->first();

        if (!$finance) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('finance');
        }else{
            return view('finance.edit_finance')->with(compact('finance'));
        }
    
    }

    function updateFinance($id , Request $request){

        $validator = Validator::make($request->all() , [
            'name' => 'required',
        ]);
        
        if ($validator->passes()) {
            $data = array(
                        'finance_name' => $request->name, 
                    );
    
            DB::table('finances')->where('id', $id)->update($data);

            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');
            return redirect('finance');

        }else{
            return redirect('finance/edit/'.$id)->withErrors($validator)->withInput();
        }
    }

    function delFinance($id, Request $request){

        $finance = DB::table('finances')->where('id', $id)->delete();

        if (!$finance) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('finance');
        }else{
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد حذف کیا جا چکا ہے۔');            
            return redirect('finance');
        }
    
    }


}
