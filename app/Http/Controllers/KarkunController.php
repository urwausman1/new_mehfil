<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Karkun;


class KarkunController extends Controller{

    public function showData(){

        $all_karkun = DB::table('karkuns')->get();
        return view('karkun.karkun')->with(compact('all_karkun'));
    }
    
    public function addKarkunPage(){
        
        $all_mahafil = DB::table('mehfils')->get();
        return view('karkun.add_karkun')->with(compact('all_mahafil'));
    }

    public function insertKarkun(Request $request){
        
        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'mehfilname' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data_admin = array(
                        'admin_username' => $request->name , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'karkun' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            // dd($last);

            // dd($last->id);
            $data = array(
                        'kp_name' => $request->name, 
                        'kp_fname' => $request->fname, 
                        'kp_phone' => $request->phone, 
                        'kp_email' => $request->email, 
                        'kp_address' => $request->address, 
                        'kp_city' => $request->city, 
                        'kp_country' => $request->country, 
                        'kp_cnic' => $request->cnic, 
                        'kp_dob' => $request->dob, 
                        'kp_doe' => $request->doe, 
                        'kp_marfat' => $request->marfat, 
                        'mehfil_id' => $request->mehfilname, 
                        'admin_id' => $last->id, 
                    );
    
            DB::table('karkuns')->insert($data);

            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('karkun/add');

        }else{
            return redirect('karkun/add')->withErrors($validator)->withInput();
        }
    }


    function editKarkun($id, Request $request){


        // dd($id);
        $all_mahafil = DB::table('mehfils')->get();

        $single_karkun = DB::table('karkuns')
                                    // ->join('mehfils', 'karkuns.mehfil_id' , '=', 'mehfils.id')
                                    ->where('karkuns.id', $id)
                                    ->first();


        // $single_karkun = DB::select("SELECT * FROM karkuns INNER JOIN mehfils ON mehfils.id = karkuns.mehfil_id WHERE karkuns.id = $id ");

        $mehfil_ka_name = DB::table('mehfils')->where('id' , $single_karkun->mehfil_id)->first();
        $mehfil_ka_name_final = $mehfil_ka_name->mehfil_name;


        if (!$single_karkun) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('karkun');
        }else{
            // dd($single_karkun);
            return view('karkun.edit_karkun')->with(compact(['single_karkun' , 'all_mahafil']))->with('mehfil_ka_name' , $mehfil_ka_name_final);
        }
    
    }

    function updateKarkun($id , Request $request){



        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'mehfilname' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data = array(
                        'kp_name' => $request->name, 
                        'kp_fname' => $request->fname, 
                        'kp_phone' => $request->phone, 
                        'kp_email' => $request->email, 
                        'kp_address' => $request->address, 
                        'kp_city' => $request->city, 
                        'kp_country' => $request->country, 
                        'kp_cnic' => $request->cnic, 
                        'kp_dob' => $request->dob, 
                        'kp_doe' => $request->doe, 
                        'kp_marfat' => $request->marfat, 
                        'mehfil_id' => $request->mehfilname, 
                    );
    
            DB::table('karkuns')->where('id' , $id)->update($data);


            // get the last id to update the data
            $getIdForUpdate = DB::table('karkuns')->where('id' , $id)->first();

            // dd($getIdForUpdate);

            $data_admin = array(
                        'admin_username' => $request->name , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'karkun' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->where('id' , $getIdForUpdate->admin_id)->update($data_admin);


            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');    
            return redirect('karkun');

        }else{
            return redirect('karkun/edit/'.$id)->withErrors($validator)->withInput();
        }
    }

}
