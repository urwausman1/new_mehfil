<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Committees;

class CommitteesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function showData(){

        $all_committees = DB::table('committees')->get();
        return view('committee.committees')->with(compact('all_committees'));
    }

    
    public function addCommitteesPage(){
        return view('committee.add_committees');
    }


    public function insertCommittee(Request $request){

        $validator = Validator::make($request->all() , [
            'name' => 'required',
        ]);
        
        if ($validator->passes()) {
            $data = array(
                        'committee_name' => $request->name, 
                    );
    
            DB::table('committees')->insert($data);

            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('committees/add');

        }else{
            return redirect('committees/add')->withErrors($validator)->withInput();
        }
    }


    function editCommittee($id, Request $request){

        $committees = DB::table('committees')->where('id', $id)->first();

        if (!$committees) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('committees');
        }else{
            return view('committee.edit_committees')->with(compact('committees'));
        }
    
    }

    function updateCommittee($id , Request $request){

        $validator = Validator::make($request->all() , [
            'name' => 'required',
        ]);
        
        if ($validator->passes()) {
            $data = array(
                        'committee_name' => $request->name, 
                    );
    
            DB::table('committees')->where('id', $id)->update($data);

            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');
            return redirect('committees');

        }else{
            return redirect('committees/edit/'.$id)->withErrors($validator)->withInput();
        }
    }

    function delCommittee($id, Request $request){

        $committees = DB::table('committees')->where('id', $id)->delete();

        if (!$committees) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('committees');
        }else{
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد حذف کیا جا چکا ہے۔');            
            return redirect('committees');
        }
    
    }
}
