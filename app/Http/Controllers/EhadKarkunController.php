<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\EhadKarkun;

class EhadKarkunController extends Controller{

    public function showData(){

        $all_ehd_karkun = DB::table('ehad_karkuns')->get();
        return view('ehad_karkun.ehad_karkun')->with(compact('all_ehd_karkun'));
    }
    
    public function addEhadKarkunPage(){
        
        return view('ehad_karkun.add_ehad_karkun');
    }

    public function insertEhadKarkun(Request $request){
        
        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'city' => 'required',
            'country' => 'required',
            'dob' => 'required',
            'doe' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data_admin = array(
                        'admin_username' => $request->name , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'ehdkarkun' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->insert($data_admin);
            $last = DB::table('users')->orderBy('id', 'desc')->first();

            // dd($last);

            // dd($last->id);
            $data = array(
                        'ekp_name' => $request->name, 
                        'ekp_fname' => $request->fname, 
                        'ekp_phone' => $request->phone, 
                        'ekp_email' => $request->email, 
                        'ekp_address' => $request->address, 
                        'ekp_city' => $request->city, 
                        'ekp_country' => $request->country, 
                        'ekp_dob' => $request->dob, 
                        'ekp_doe' => $request->doe, 
                        'ekp_marfat' => $request->marfat, 
                        'ekp_cnic' => $request->cnic, 
                        'admin_id' => $last->id, 
                    );
    
            DB::table('ehad_karkuns')->insert($data);

            $request->session()->flash('msg' , 'معلومات کا اندراج ہو چکا ہے.');
    
            return redirect('ehadkarkun/add');

        }else{
            return redirect('ehadkarkun/add')->withErrors($validator)->withInput();
        }
    }


    function editEhadKarkun($id, Request $request){

        $single_ehdkarkun = DB::table('ehad_karkuns')->where('id', $id)->first();

        if (!$single_ehdkarkun) {
            $request->session()->flash('msg' , 'آپکا مطلوبہ مواد موجود نہیں ہے۔');
            return redirect('ehadkarkun');
        }else{
            return view('ehad_karkun.edit_ehad_karkun')->with(compact('single_ehdkarkun'));
        }
    
    }

    function updateEhadKarkun($id , Request $request){



        $validator = Validator::make($request->all() , [
            'name' => 'required',
            'fname' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'cnic' => 'required',
            'address' => 'required',
            'city' => 'required',
            'doe' => 'required',
            'dob' => 'required',
            'country' => 'required',
        ]);
        
        if ($validator->passes()) {

            $data = array(
                        'ekp_name' => $request->name, 
                        'ekp_fname' => $request->fname, 
                        'ekp_phone' => $request->phone, 
                        'ekp_email' => $request->email, 
                        'ekp_address' => $request->address, 
                        'ekp_city' => $request->city, 
                        'ekp_country' => $request->country, 
                        'ekp_dob' => $request->dob, 
                        'ekp_doe' => $request->doe, 
                        'ekp_marfat' => $request->marfat, 
                        'ekp_cnic' => $request->cnic, 
                    );
    
            DB::table('ehad_karkuns')->where('id' , $id)->update($data);


            // get the last id to update the data
            $getIdForUpdate = DB::table('ehad_karkuns')->where('id' , $id)->first();

            $data_admin = array(
                        'admin_username' => $request->name , 
                        'admin_email' => $request->email , 
                        'admin_password' => 'usman' , 
                        'admin_access_level' => 'ehdkarkun' , 
                        'admin_status' => 1 , 
            );

            DB::table('users')->where('id' , $getIdForUpdate->admin_id)->update($data_admin);


            $request->session()->flash('msg' , 'معلومات میں تبدیلی کر دی گیئ ہے۔');    
            return redirect('ehadkarkun');

        }else{
            return redirect('ehadkarkun/edit/'.$id)->withErrors($validator)->withInput();
        }
    }
    

}
