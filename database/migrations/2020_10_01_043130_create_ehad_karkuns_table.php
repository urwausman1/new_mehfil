<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEhadKarkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ehad_karkuns', function (Blueprint $table) {
            $table->id();
            $table->string('ekp_name');
            $table->string('ekp_fname');
            $table->string('ekp_phone');
            $table->string('ekp_email');
            $table->string('ekp_address');
            $table->string('ekp_city');
            $table->string('ekp_country');
            $table->string('ekp_cnic');
            $table->string('ekp_dob');
            $table->string('ekp_doe');
            $table->string('ekp_marfat');
            $table->integer('admin_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ehad_karkuns');
    }
}
