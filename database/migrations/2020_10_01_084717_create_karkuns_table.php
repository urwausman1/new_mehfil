<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKarkunsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karkuns', function (Blueprint $table) {
            $table->id();
            $table->string('kp_name');
            $table->string('kp_fname');
            $table->string('kp_address');
            $table->string('kp_city');
            $table->string('kp_country');
            $table->string('kp_cnic');
            $table->string('kp_phone');
            $table->string('kp_email');
            $table->string('kp_dob');
            $table->string('kp_doe');
            $table->string('kp_notes')->nullable();
            $table->string('kp_marfat')->nullable();
            $table->string('committee_id')->nullable();
            $table->string('mehfil_id');
            $table->integer('admin_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karkuns');
    }
}
