<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMehfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mehfils', function (Blueprint $table) {
            $table->id();
            $table->string('mehfil_number');
            $table->string('mehfil_code');
            $table->string('mehfil_name');
            $table->string('mehfil_email');
            $table->string('mehfil_address');
            $table->string('mehfil_city');
            $table->string('mehfil_country');
            $table->string('mehfil_media_cell_phone');
            $table->string('mehfil_mobile');
            $table->string('mehfil_coordinates');
            $table->string('admin_id');
            $table->integer('ekp_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mehfils');
    }
}
