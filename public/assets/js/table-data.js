var TableData = function () {

    //function to initiate DataTable

    //DataTable is a highly flexible tool, based upon the foundations of progressive enhancement, 

    //which will add advanced interaction controls to any HTML table

    //For more information, please visit https://datatables.net/

    var runDataTable = function () {



        

        var oTable = $('#contacts_table').dataTable({

            "aoColumnDefs": [{

                "aTargets": [0]

            }],

            "oLanguage": {

                "sLengthMenu": "سطریں _MENU_ ",

                "sSearch": "",

                "sEmptyTable": "یہ ٹیبل خالی ہے",

                "oPaginate": {

                    "sPrevious": "",

                    "sNext": ""

                }

            },

            "aaSorting": [

                [1, 'asc']

            ],

            "aLengthMenu": [

                [10, 20, 30, 40, 50, -1],

                [10, 20, 30, 40, 50, "تمام"] // change per page values here

            ],

            // set the initial value

            "iDisplayLength": 50,

        });

        //$('#contacts_table_wrapper .dataTables_filter input').addClass("form-control input-sm").attr("placeholder", "Search");

        // modify table search input

        //$('#contacts_table_wrapper .dataTables_length select').addClass("m-wrap small");

        // modify table per page dropdown

       // $('#contacts_table_wrapper .dataTables_length select').select2();

        // initialzie select2 dropdown

        $('#contacts_table_column_toggler input[type="checkbox"]').change(function () {

            /* Get the DataTables object again - this is not a recreation, just a get of the object */

            var iCol = parseInt($(this).attr("data-column"));

            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;

            oTable.fnSetColumnVis(iCol, (bVis ? false : true));

        });

    };

    return {

        //main function to initiate template pages

        init: function () {

            runDataTable();

        }

    };

}();