var FormValidator = function () {

    // function to initiate Validation Sample 1

    var runValidator_add_contact = function () {

        var add_contact = $('#add_contact');

        var errorHandler_add_contact = $('.errorHandler', add_contact);

        var successHandler_add_contact = $('.successHandler', add_contact);

        

	

		 $.validator.addMethod("FullDate", function () {

            //if all values are selected

            if ($("#yyyy").val() != "" && $("#mm").val() != "" ) {

                return true;

            } else {

                return false;

            }

        }, ' عرصہ عہد بتائیں‬‎‬‎‬‎');

		$.validator.addMethod("FullDate2", function () {

            //if all values are selected

            if ($("#YEAR").val() != "" && $("#MONTH").val() != "" && $("#DAY").val() != ""  ) {

                return true;

            } else {

                return false;

            }

        }, ' تاریخ پدائش بتائیں‬‎‬‎‬‎‬‎');

		

		

		



        $('#add_contact').validate({

            errorElement: "span", // contain the error msg in a span tag

            errorClass: 'help-block',

            errorPlacement: function (error, element)

			 { // render error placement for each input type

                if (element.attr("type") == "radio" || element.attr("type") == "checkbox") 

				{ // for chosen elements, need to insert the error after the chosen container

                    error.insertAfter($(element).closest('.form-group').children('div').children().last());

                } 

				

				 

				else if (element.hasClass("hasicn")) {

                    error.insertAfter($(element).closest('.form-group').children('div'));

                } 

				else 

				{

                    error.insertAfter(element);

                    // for other inputs, just perform default behavior

                }

            },



            ignore: "",

            rules: {

                name: {

                    minlength: 4,

                    required: true

                },

				address: {

                    minlength: 6

                },

				// city: {

    //                 minlength: 2

    //             },

				marfat: {

                    minlength: 2

                },

                email: {

                    minlength: 4

                },

               

                  

               

              

				waldiyat: {

                    minlength: 2

                },  

				// yyyy: "FullDate",

				// YEAR: "FullDate2",

                phone: {

                    minlength: 11,

                    required: true

                },

                email: {

                    minlength: 2

                },

               

               

            },

            messages: {

                name:    " نام بتائیں‬‎ ",

                address: " پتہ بتائیں‬‎",

                city: " شہر بتائیں‬‎",

                country: " ملک بتائیں‬‎",

                marfat: "  معر فت بتائیں‬‎",

				waldiyat: "  ولدیت بتائیں‬‎",

               

                cnic: {

					 minlength:  "شناختی کارڈ نمبر رست نھیں‬‎"

				},

               

                phone: " رابطہ نمبر بتائیں‬‎",

                email: {

                    

                    email: " ای-میل درست نھیں"

                },

             

                

                

            },

             groups: {

                DateofBirth: "dd mm yyyy",

            },

            

            invalidHandler: function (event, validator) { //display error alert on form submit

                successHandler_add_contact.hide();

                errorHandler_add_contact.show();

            },

            highlight: function (element) {

                $(element).closest('.help-block').removeClass('valid');

                // display OK icon

                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');

                // add the Bootstrap error class to the control group

            },

            unhighlight: function (element) { // revert the change done by hightlight

                $(element).closest('.form-group').removeClass('has-error');

                // set error class to the control group

            },

            success: function (label, element) {

                label.addClass('help-block valid');

                // mark the current input as valid and display OK icon

                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');

            },

            

        });

    };

    // function to initiate Validation Sample 2

    var runValidator2 = function () {};

    return {

        //main function to initiate template pages

        init: function () {

            runValidator_add_contact();

            

        }

    };

}();